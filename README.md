# tic-tac-toe

## Project setup
- cd to node_server/
- run npm install
- run npm start and leave server instance running
- cd to tic-tac-toe/
- run npm install
- run npm run serve
- access app at http://localhost:8080

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
