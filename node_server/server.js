const server = require('server')
const { get, socket } = require('server/router')

const port = process.env.PORT || 3000
server({ port }, [
    get('/', sio => ''),
    socket('clientJoined', sio => {
        const count = sio.io.engine.clientsCount;
        sio.io.emit('joinAlert', {id: sio.data, count: count});
    }),
    socket('clientLeft', sio => {
        const count = sio.io.engine.clientsCount;
        sio.io.emit('updateCountAndQue', {player: sio.data, count: count});
    }),
    socket('playerPicked', sio => {
        const count = sio.io.engine.clientsCount;
        sio.io.emit('updateGrid', sio.data);
    }),
    socket('checkPlayers', sio => {
        const count = sio.io.engine.clientsCount;
        sio.io.emit('playersCheck', sio.data);
    }),
    socket('restartGame', sio => {
        sio.io.emit('restartGame', {});
    }),
    socket('disconnect', sio => {
        const count = sio.io.engine.clientsCount;
        sio.io.emit('updateCount', {player: sio.data, count: count});
    }),
])
    .then(() => console.log(`Server runnin at http://localhost:${port}`))



