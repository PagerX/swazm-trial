export const EventBus = new Vue();
import Vue from 'vue';
import App from './App.vue';
import router from './router.js';
import VueSocketIO from 'vue-socket.io'


Vue.use(new VueSocketIO({
    debug: false,
    connection: 'http://localhost:3000'
}));

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount('#app');


